// @ts-check
// BEGIN
import faker from 'faker';

const APP_URL = 'http://localhost:8080';
const timeout = 30000;

const selectors = {
  input: 'input[data-testid="task-name-input"]',
  buttonAdd: 'input[data-testid="add-task-button"]',
  deleteTask1: '[data-testid="remove-task-1"]',
};

describe('E2E best practice', () => {
  beforeEach(async () => {
    await page.goto(APP_URL);
  });

  it(
    'Main page opens',
    async () => {
      await expect(page).toMatchElement(selectors.buttonAdd);
      await expect(page).toMatchElement(selectors.input);
    },
    timeout,
  );

  it(
    'Add tasks',
    async () => {
      const text = faker.lorem.word();
      await page.waitForSelector(selectors.buttonAdd);
      await expect(page).toFillForm('form', { text });

      await page.click(selectors.buttonAdd);
      await expect(page).toMatch(text);
    },
    timeout,
  );

  it(
    'Remove task',
    async () => {
      const text = faker.lorem.word();
      await page.waitForSelector(selectors.buttonAdd);
      await expect(page).toFillForm('form', { text });

      await page.click(selectors.buttonAdd);
      await expect(page).toMatch(text);

      await page.waitForSelector(selectors.deleteTask1);
      await page.click(selectors.deleteTask1);
      await page.waitForSelector('body', { visible: true });
      const liCounts = await page.$$eval('li', (li) => li.length);
      await expect(liCounts).toEqual(0);
      await expect(page).not.toMatch(text);
    },
    timeout,
  );
});

// END
