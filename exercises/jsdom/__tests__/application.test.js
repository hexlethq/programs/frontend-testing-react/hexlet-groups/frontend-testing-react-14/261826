// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const faker = require('faker');

const run = require('../src/application');

const selectors = {
  inputList: '[data-testid="add-list-input"]',
  addList: '[data-testid="add-list-button"]',
  inputTask: '[data-testid="add-task-input"]',
  addTask: '[data-testid="add-task-button"]',
  blockLists: '[data-container="lists"]',
  blockTasks: '[data-container="tasks"]',
  specificList: '[data-testid="myListName-list-item"]',
};

const addNewList = (listName) => {
  document.querySelector(selectors.inputList).value = listName;
  document.querySelector(selectors.addList).click();
};

const addNewTaskToCurrentList = (taskName) => {
  document.querySelector(selectors.inputTask).value = taskName;
  document.querySelector(selectors.addTask).click();
};
const switchToList = (myListName) => {
  const selector = selectors.specificList.replace('myListName', myListName);
  document.querySelector(selector).click();
};
const getTaskBlock = () => document.querySelector(selectors.blockTasks);
const getListBlock = () => document.querySelector(selectors.blockLists);

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
describe('jsdom', () => {
  test('There is h2 with text Simple Todo List', () => {
    const h2 = document.querySelector('h2');
    expect(h2).not.toBeNull();
    expect(h2.textContent).toBe('Simple Todo List');
  });

  it('Check init data. Task list is empty, there is the one list "General"', () => {
    const taskBlock = getTaskBlock();
    const listBlock = getListBlock();

    expect(taskBlock).toBeEmptyDOMElement();
    expect(listBlock).toHaveTextContent('General');
  });

  it('should assign default list General for a new task', () => {
    // Задача добавляется в список по умолчанию (General).

    const newTaskName = faker.name.title();
    const newList = faker.lorem.word();
    const taskBlock = getTaskBlock();

    addNewTaskToCurrentList(newTaskName);
    expect(taskBlock).toHaveTextContent(newTaskName);

    addNewList(newList);
    switchToList(newList);
    expect(taskBlock).not.toHaveTextContent(newTaskName);
  });

  it('should clear text input of task after adding new task and display the new task in Task list', () => {
    const newTaskName = faker.name.title();
    addNewTaskToCurrentList(newTaskName);
    const inputTask = document.querySelector(selectors.inputTask);

    expect(inputTask).not.toHaveValue(newTaskName);
    expect(inputTask.value).toBe('');
    expect(getTaskBlock()).toHaveTextContent(newTaskName);
  });

  it('should display all added tasks', () => {
    const taskNames = [1, 2, 3].map(() => faker.name.jobTitle());
    const taskBlock = getTaskBlock();
    taskNames.forEach((taskName) => addNewTaskToCurrentList(taskName));

    taskNames.forEach((taskName) => {
      expect(taskBlock).toHaveTextContent(taskName);
    });
  });

  it('should display correct tasks for selected list and dont display tasks from another list', () => {
    const tasksOfGeneralList = [1, 2, 3].map(() => faker.name.jobTitle());
    const tasksOfNewList = [1, 2, 3].map(() => faker.name.jobTitle());
    const newListName = faker.lorem.word();
    const taskBlock = getTaskBlock();

    tasksOfGeneralList.forEach((taskName) => addNewTaskToCurrentList(taskName));
    addNewList(newListName);
    switchToList(newListName);
    tasksOfNewList.forEach((taskName) => addNewTaskToCurrentList(taskName));

    switchToList('general');
    tasksOfGeneralList.forEach((taskName) => {
      expect(taskBlock).toHaveTextContent(taskName);
    });
    tasksOfNewList.forEach((taskName) => {
      expect(taskBlock).not.toHaveTextContent(taskName);
    });

    switchToList(newListName);
    tasksOfGeneralList.forEach((taskName) => {
      expect(taskBlock).not.toHaveTextContent(taskName);
    });
    tasksOfNewList.forEach((taskName) => {
      expect(taskBlock).toHaveTextContent(taskName);
    });
  });
});
// END
