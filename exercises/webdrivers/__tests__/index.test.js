const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;

let browser;
let page;

const app = getApp();

describe('it works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });
  // BEGIN
  test('When I open main page, it should be titled "Simple blog"', async () => {
    await page.goto(appUrl);
    await expect(page.title()).resolves.toMatch('Simple Blog');
  });

  test('When I open page with articles, I should get something with tag "articles"', async () => {
    await page.goto(`${appUrl}/articles`);
    const articles = await page.$('#articles');
    expect(articles).toBeTruthy();
  });

  test('when I click on anchor "Create new article", I should go to page with form', async () => {
    await page.goto(`${appUrl}/articles`);
    const selector = '.container > a';

    await Promise.all([page.waitForNavigation(), page.click(selector)]);

    expect(page.url()).toBe(`${appUrl}/articles/new`);

    const form = await page.$('form');
    expect(form).toBeTruthy();
  });

  test(
    "I can data in form for new article and click 'Create', "
      + 'then I should be transferred to articles page '
      + 'and see the created article',
    async () => {
      const optionsTimeout = { timeout: 30000 };
      jest.setTimeout(30000);

      await page.goto(`${appUrl}/articles/new`, optionsTimeout);
      await page.type('#name', 'Some text in title');
      await page.select('#category', '1');
      await page.type('#content', 'Some text in article content');

      await Promise.all([
        page.waitForNavigation(optionsTimeout),
        page.click('.btn-primary'),
      ]);

      expect(page.url()).toBe(`${appUrl}/articles`);
      const articles = await page.$$eval('tr > td', (cells) => cells.map((cell) => cell.textContent === 'Some text in title'));
      expect(articles.length).toBeGreaterThanOrEqual(1);
    },
  );
  // END
  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
