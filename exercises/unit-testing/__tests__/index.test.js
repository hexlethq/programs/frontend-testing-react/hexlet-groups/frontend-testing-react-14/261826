describe('Object.assign()', () => {
  test('main', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src);

    // BEGIN
    // expect(result).toBe({ k: 'v', a: 'a', b: 'b' }); // Test fail
    expect(result).toEqual({ k: 'v', a: 'a', b: 'b' });
    expect(result).toBe(target);

    // END
  });

  test('Cloning an object', () => {
    const obj = { a: 1 };
    const copy = { ...obj };
    expect(copy).toEqual({ a: 1 });
  });

  test('Merging objects', () => {
    const o1 = { a: 1 };
    const o2 = { b: 2 };
    const o3 = { c: 3 };

    const obj = Object.assign(o1, o2, o3);
    expect(obj).toEqual({ a: 1, b: 2, c: 3 });

    expect(o1).toEqual({ a: 1, b: 2, c: 3 });
  });

  test('Merging objects with same properties', () => {
    const src = { x: 0 };
    const o1 = { a: 1, b: 1, c: 1 };
    const o2 = { b: 2, c: 2 };
    const o3 = { c: 3 };

    const obj = Object.assign(src, o1, o2, o3);
    expect(obj).toEqual({
      x: 0,
      a: 1,
      b: 2,
      c: 3,
    });
  });

  test('Properties on the prototype chain and non-enumerable properties cannot be copied', () => {
    const obj = Object.create(
      { foo: 1 },
      {
        // foo is on obj's prototype chain.
        bar: {
          value: 2, // bar is a non-enumerable property.
        },
        baz: {
          value: 3,
          enumerable: true, // baz is an own enumerable property.
        },
      },
    );

    const emptyObj = {};
    const copy = Object.assign(emptyObj, obj);
    expect(copy).toEqual({ baz: 3 });
  });

  test('Primitives will be wrapped to objects', () => {
    const v1 = 'abc';
    const v2 = true;
    const v3 = 10;
    const v4 = Symbol('foo');
    const emptyObj = {};

    const obj = Object.assign(emptyObj, v1, null, v2, undefined, v3, v4);
    // Primitives will be wrapped, null and undefined will be ignored.
    // Note, only string wrappers can have own enumerable properties.

    expect(obj).toEqual({ 0: 'a', 1: 'b', 2: 'c' });
  });

  test('Exceptions will interrupt the ongoing copying task', () => {
    const target = Object.defineProperty({}, 'foo', {
      value: 1,
      writable: false,
    }); // target.foo is a read-only property

    const errorFunc = () => {
      Object.assign(
        target,
        { bar: 2 },
        { foo2: 3, foo: 3, foo3: 3 },
        { baz: 4 },
      );
    };
    expect(errorFunc).toThrowError();
  });
});
