const faker = require('faker');

// BEGIN
describe('faker', () => {
  test('createTransaction', () => {
    const transaction = faker.helpers.createTransaction();

    expect(transaction).toMatchObject({
      account: expect.any(String),
      amount: expect.any(String),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
    });
  });

  test('All transactions are unique', () => {
    expect(faker.helpers.createTransaction()).not.toMatchObject(
      faker.helpers.createTransaction(),
    );
    expect(faker.helpers.createTransaction()).not.toBe(
      faker.helpers.createTransaction(),
    );
  });
});
// END
