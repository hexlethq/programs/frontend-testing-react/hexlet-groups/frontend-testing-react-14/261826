const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('Array sort. Example-based', () => {
  test('numbers', () => {
    expect(sort([40, 1, 5, 200])).toEqual([1, 5, 40, 200]);
  });
  test('numeric strings', () => {
    expect(sort(['80', '9', '700'])).toEqual(['9', '80', '700']);
  });
  test('strings array. Doesnt sort', () => {
    const src = ['Blue', 'Humpback', 'Beluga'];
    const result = ['Blue', 'Humpback', 'Beluga'];
    expect(sort(src)).toEqual(result);
  });
  test('mixed numeric', () => {
    const src = ['80', '9', '700', 40, 1, 5, 200];
    const result = [1, 5, '9', 40, '80', 200, '700'];
    expect(sort(src)).toEqual(result);
  });
});

describe('Array sort. Property-based', () => {
  test('should contain the same items', () => {
    const count = (tab, element) => tab.filter((v) => v === element).length;
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted.length).toEqual(data.length);
        data.forEach((item) => expect(count(sorted, item)).toEqual(count(data, item)));
      }),
    );
  });

  test('should produce ordered array', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted).toBeSorted();
      }),
    );
  });

  test('compare length', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(data.length).toBe(sorted.length);
      }),
    );
  });
  test('double sort', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        expect(sort(data)).toStrictEqual(sort(sort(data)));
      }),
    );
  });
});
// END
