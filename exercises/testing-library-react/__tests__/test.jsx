// @ts-check

import '@testing-library/jest-dom';

import nock from 'nock';
import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Autocomplete from '../src/Autocomplete.jsx';

const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

// BEGIN
afterAll(() => {
  nock.enableNetConnect();
});

beforeEach(() => {
  nock(host)
    .get('/countries')
    .query({ term: 'i' })
    .reply(200, [
      'Indonesia',
      'Ireland',
      'Israel',
      'Isle of Man',
      'India',
      'Iraq',
      'Iran, Islamic Republic of',
      'Iceland',
      'Italy',
    ]);
  nock(host)
    .get('/countries')
    .query({ term: 'ir' })
    .reply(200, ['Ireland', 'Iraq', 'Iran, Islamic Republic of']);
  nock(host).get('/countries').query({ term: 'ire' }).reply(200, ['Ireland']);
  nock(host).get('/countries').query({ term: 'irec' }).reply(200, []);
});

describe('Autocomplete country name', () => {
  it('should show appropriate countries if type one symbol i', async () => {
    const { getByPlaceholderText, queryByText, getByRole } = render(<Autocomplete />);
    expect(getByPlaceholderText('Enter Country')).toBeInTheDocument();
    userEvent.type(getByRole('textbox'), 'i');
    await waitFor(() => {
      expect(queryByText('Indonesia')).toBeVisible();
      expect(queryByText('Ireland')).toBeVisible();
      expect(queryByText('Israel')).toBeVisible();
    });
  });

  it('should display less countries, if I type 2 symbols ir  ', async () => {
    const { getByPlaceholderText, queryByText, getByRole } = render(<Autocomplete />);
    expect(getByPlaceholderText('Enter Country')).toBeInTheDocument();
    userEvent.type(getByRole('textbox'), 'i');
    userEvent.type(getByRole('textbox'), 'r');
    await waitFor(() => {
      expect(queryByText('Ireland')).toBeVisible();
      expect(queryByText('Iran, Islamic Republic of')).toBeVisible();
      expect(queryByText('Italy')).toBeNull();
    });
  });

  it('should display only 1 country Ireland, if I type ire', async () => {
    const { queryByText, getByRole } = render(<Autocomplete />);
    userEvent.type(getByRole('textbox'), 'i');
    userEvent.type(getByRole('textbox'), 'r');
    userEvent.type(getByRole('textbox'), 'e');
    await waitFor(() => {
      expect(queryByText('Ireland')).toBeVisible();
      expect(queryByText('Iran')).toBeNull();
    });
  });

  it('should display nothing, if I clear input', async () => {
    const { queryByText, getByRole } = render(<Autocomplete />);
    userEvent.clear(getByRole('textbox'));
    await waitFor(() => {
      expect(queryByText('Ireland')).toBeNull();
    });
  });
});
// END
