// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const testingLibraryDom = require('@testing-library/dom');
const testingLibraryUserEvent = require('@testing-library/user-event');
const faker = require('faker');
const run = require('../src/application');

const { screen, configure } = testingLibraryDom;
const userEvent = testingLibraryUserEvent.default;

configure({
  testIdAttribute: 'data-container',
});

const addNewList = (listName) => {
  userEvent.type(screen.getByRole('textbox', { name: /new list/i }), listName);
  userEvent.click(screen.getByRole('button', { name: /add list/i }));
};

const addNewTaskToCurrentList = (taskName) => {
  userEvent.type(screen.getByRole('textbox', { name: /new task/i }), taskName);
  userEvent.click(screen.getByRole('button', { name: /add task/i }));
};
const switchToList = (myListName) => {
  userEvent.click(screen.getByText(new RegExp(myListName, 'i')));
};

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});
// BEGIN
describe('Testing app Simple Todo List by testing-library-dom', () => {
  it('should have empty task list and default list General', () => {
    expect(screen.getByTestId('tasks')).toBeEmptyDOMElement();
    expect(screen.getByText(/general/i)).toBeVisible();
  });

  it('should add new task in default list with name general', () => {
    const taskName = faker.commerce.productName();

    addNewTaskToCurrentList(taskName);

    expect(screen.getByText(/general/i)).toBeVisible();
    expect(screen.getByText(new RegExp(taskName))).toBeVisible();
  });

  it('should clear the input field after adding a new task and display new task', () => {
    const taskName = faker.lorem.word();

    addNewTaskToCurrentList(taskName);

    expect(screen.getByRole('textbox', { name: /new task/i })).toHaveValue('');
    expect(screen.getByText(taskName)).toBeInTheDocument();
  });

  it('should display all added tasks', () => {
    const tasks = [faker.lorem.word(), faker.lorem.word(), faker.lorem.word()];

    tasks.forEach((task) => addNewTaskToCurrentList(task));

    tasks.forEach((task) => expect(screen.getByText(task)).toBeInTheDocument());
  });

  it('should display tasks belong selected list', () => {
    const tasksGeneral = [faker.lorem.word(), faker.lorem.word(), faker.lorem.word()];
    const tasksNewList = [faker.lorem.word(), faker.lorem.word(), faker.lorem.word()];
    const newListName = faker.commerce.productName();

    tasksGeneral.forEach((task) => addNewTaskToCurrentList(task));
    addNewList(newListName);
    switchToList(newListName);
    tasksNewList.forEach((task) => addNewTaskToCurrentList(task));
    switchToList('general');

    tasksGeneral.forEach((task) => expect(screen.getByText(task)).toBeInTheDocument());
    tasksNewList.forEach((task) => expect(screen.queryAllByText(task).length).toBe(0));

    switchToList(newListName);

    tasksNewList.forEach((task) => expect(screen.getByText(task)).toBeInTheDocument());
    tasksGeneral.forEach((task) => expect(screen.queryAllByText(task).length).toBe(0));
  });
});

// END
