// @ts-check

import React from 'react';
import nock from 'nock';
import axios from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';

import TodoBox from '../src/TodoBox.jsx';

axios.defaults.adapter = httpAdapter;
const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

afterAll(() => {
  nock.enableNetConnect();
});

// BEGIN
afterEach(() => {
  nock.cleanAll();
});

describe('Thing list to do application', () => {
  it('should have empty list in initial start', async () => {
    nock(host).get('/tasks').reply(200, []);
    const { getByPlaceholderText, queryByText } = render(<TodoBox />);

    expect(getByPlaceholderText('I am going...')).toBeInTheDocument();
  });

  it('should display new task after adding and display both after adding two tasks ', async () => {
    const taskName1 = 'NewTask1';
    const taskName2 = 'NewTask2';

    nock(host).get('/tasks').reply(200, []);
    nock(host)
      .post('/tasks', { text: taskName1 })
      .reply(200, { id: 1, text: taskName1, state: 'active' });
    nock(host)
      .post('/tasks', { text: taskName2 })
      .reply(200, { id: 2, text: taskName2, state: 'active' });
    render(<TodoBox />);
    const input = screen.getByPlaceholderText('I am going...');
    const submit = screen.getByRole('button', { name: 'add' });

    userEvent.type(input, taskName1);
    userEvent.click(submit);
    expect(await screen.findByText(taskName1)).toBeVisible();

    userEvent.type(input, taskName2);
    userEvent.click(submit);
    expect(await screen.findByText(taskName2)).toBeVisible();
    expect(await screen.findByText(taskName2)).toBeVisible();
  });

  it('should work correctly when I undo finishing', async () => {
    const taskName1 = 'NewTask1';
    nock(host)
      .get('/tasks')
      .reply(200, [{ id: 1, text: taskName1, state: 'active' }]);
    nock(host).patch('/tasks/1/finish').reply(200, { id: 1, text: taskName1, state: 'finished' });
    nock(host).patch('/tasks/1/activate').reply(200, { id: 1, text: taskName1, state: 'active' });
    const { container } = render(<TodoBox />);
    const task = await screen.findByText(taskName1);
    expect(task).toBeVisible();

    userEvent.click(task);
    await waitFor(() => {
      expect(container.querySelector('s')).toBeVisible();
    });

    userEvent.click(await screen.findByText(taskName1));
    await waitFor(() => {
      expect(container.querySelector('s')).toBeNull();
    });
  });
});
// END
