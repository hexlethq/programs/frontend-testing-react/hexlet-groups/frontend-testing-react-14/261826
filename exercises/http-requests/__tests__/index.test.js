const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN
const URL_API = 'http://www.example.com';
const user = {
  firstname: 'Fedor',
  lastname: 'Sumkin',
  age: 33,
  id: 1,
};
const answer = [
  user,
  {
    firstname: 'Egor',
    lastname: 'Morozov',
    age: 18,
    id: 17,
  },
];
beforeAll(() => {
  nock.disableNetConnect();
  nock.enableNetConnect(URL_API);
});
afterEach(() => {
  nock.cleanAll();
});
afterAll(() => {
  nock.enableNetConnect();
});
describe('http-requests', () => {
  test('when create user, we should get code 201', async () => {
    const scope = nock(URL_API).post('/users').reply(201, answer);
    const response = await post(`${URL_API}/users`, user);
    expect(scope.isDone()).toBeTruthy();
    expect(response.status).toBe(201);
  });

  test('when read users, should get array', async () => {
    nock(URL_API).get('/users').reply(200, answer);
    const response = await get(`${URL_API}/users`);
    expect(response.data).toEqual(answer);
  });

  test('when update user, should get code 200', async () => {
    nock(URL_API).put('/users').reply(200, answer);
    const response = await axios.put(`${URL_API}/users`, user);
    expect(response.status).toBe(200);
  });

  test('when delete user, we should get code 200', async () => {
    nock(URL_API).delete('/users/1').reply(200, answer);
    const response = await axios.delete(`${URL_API}/users/1`);
    expect(response.status).toBe(200);
  });
});
// END
