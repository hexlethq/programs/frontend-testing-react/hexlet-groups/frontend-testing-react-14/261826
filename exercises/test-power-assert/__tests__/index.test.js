const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
const array = [1, [2, [3, [4]], 5]];
const flatten1 = [1, 2, [3, [4]], 5];

assert.notEqual(flattenDepth(array), flatten1, 'Flatten depth level 1');
assert.deepStrictEqual(flattenDepth(array), flatten1, 'Flatten depth level 1');
assert.deepStrictEqual(flattenDepth(array, 5), flattenDepth(array, 4), 'Flatten depth level 4 and 5');
assert.deepStrictEqual(flattenDepth(array), flattenDepth(array, 1), 'Flatten depth level 1');
// END
